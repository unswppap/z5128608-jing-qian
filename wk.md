## Week 2
We discussed about which topic to do on Thursday lab time, but we didn't have a consensus between topic 1 and 2.  It was a good incident I think :)  It means that our ideas were diversed, all of us are creative.  Finally we made a decision days later, we would work on the first topic(i.e. the similar case search engine), then we started writing proposals.  We talked much on trello, and finished the first part at the end of the week.


## Week 3
Our work is proceeding as the schedule, we discussed about the rest part of the proposal on lab time and trello.  Thanks to our PM, we wrote a good proposal(at least I like it).  Next we reviewed(or say teach each other) some courses, because I elected COMP6714(IR), but other members elected COMP9313(Bigdata) in the past semester.  We finished reading those materials on 6714 this week, next week we should work more on 9313.


## Week 4
Xinjie found some extra materials last week, I checked them, they were very useful, and will help us a lot in the future works.  Also, I (some kind of) learned 9313 this week, some of them were not so easy, it would take time for me to understand, but fortunately, I know most of them now (I think).


## Week 5
Last week before mid break!  We've started analyzing the data Haojie obtained from NSW supreme court.  I thought 30k documents were too less, but when I started dealing with them, it became (looks) so much :(  Anyway, inversed index were created.  Next, we tried calculating tf-idf, but many problems occured, the most we need to deal with is the fact that there were too many words in the documents, the corps had been too large, it could lead to many new problems because of the limitation of our computers.


## Week 5.5
Mid break.

## Week 6
This week we started doing different works.  I'm the only full stack engineer in the team so I'll deal with the website.  We discussed about the UI design, all of us thought AUSTLII's UI was concise and simple to use, that was what we want.  I wrote the front end with bootstrap, which was a good FE framework, along with jQuery.  I've been not writing html for months, so I worked a bit slow, but I finished most of them by the end of this week.  Website FE would be finished next week.


## Week 7
FE part was finished, Yingyi also finished the similarity calculation functions, I started combining the website with the functions.  I transform Yingyi's code to a object orientation style on the package level, for the easy use.  By the end of this week, the search engine could be used to search by a few keywords, more features would be added in the future.


## Week 8
Almost all features were available now, but the codes were in a mess, they were not so easy to read, even I need to take a long time to understand what those codes mean, maybe refactor would be necessary.  But at least, The website could search by words, and sort the results in order, and show their similarities.


## Week 9
My mood was "Stick man jump down from UNSW library.png".  I factored all codes on website, it was fully object orientation now, which would be easy to read.  All features were online now, including file upload, advanced search, highlight keywords, search in a region(in date or category).  I need to thank Xinjie for her works on cluster, she succeeded in dividing the documents in 9 categories.

## Week 10
Tutor told us that we could make the file search faster if use doc2vec.  I also considered about distributed calculation but there's something wrong with our network. But finally we developed document search module.  We'll also have a meeting on Monday, hope everything will be OK in presentations.
